# AUTHOR <EMAIL@ADDRESS>, YEAR.
# translation of Feedback.po to
# Ani Peter <apeter@redhat.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2009-12-02 04:07-0500\n"
"Last-Translator: \n"
"Language-Team: <en@li.org>\n"
"Language: ml\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.6.0\n"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "We Need Feedback!"
msgstr "നിങ്ങളുടെ അഭിപ്രായം ദയവായി ഞങ്ങള്‍ക്കു് അയച്ചു തരിക!"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "<primary>feedback</primary> <secondary>contact information for this manual</secondary>"
msgstr "<primary>അഭിപ്രായം</primary> <secondary>ഈ മാനുവലിലേക്കു് ബന്ധപ്പെടുവാനുള്ള വിവരങ്ങള്‍</secondary>"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "You should over ride this by creating your own local Feedback.xml file."
msgstr "നിങ്ങള്‍ സ്വന്തമായി ഒരു Feedback.xml ഫയല്‍ ഉണ്ടാക്കി ഇതിനെ മാറ്റി എഴുതുക."

