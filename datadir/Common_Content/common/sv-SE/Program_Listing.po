# AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: \n"
"Last-Translator: Automatically generated\n"
"Language-Team: None\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.6.0\n"

msgid ""
"package org.jboss.book.jca.ex1;\n"
"\n"
"import javax.naming.InitialContext;\n"
"\n"
"public class ExClient\n"
"{\n"
"   public static void main(String args[]) \n"
"       throws Exception\n"
"   {\n"
"      InitialContext iniCtx = new InitialContext();\n"
"      Object         ref    = iniCtx.lookup(\"EchoBean\");\n"
"      EchoHome       home   = (EchoHome) ref;\n"
"      Echo           echo   = home.create();\n"
"\n"
"      System.out.println(\"Created Echo\");\n"
"\n"
"      System.out.println(\"Echo.echo('Hello') = \" + echo.echo(\"Hello\"));\n"
"   }\n"
"}"
msgstr ""

