# AUTHOR <EMAIL@ADDRESS>, YEAR.
# Publican conventons.
# Muayyad Alsadi <alsadi@ojuba.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2009-03-12 05:39-0400\n"
"Last-Translator: Muayyad Alsadi <alsadi@ojuba.org>\n"
"Language-Team: ar <core@ojuba.org>\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural= n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Zanata 3.6.0\n"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Document Conventions"
msgstr "أعراف الوثيقة"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "This manual uses several conventions to highlight certain words and phrases and draw attention to specific pieces of information."
msgstr "هذا الكتيب يستخدم عدد من الأعراف لإبراز كلمات أو جملة معينة ولجلب الانتباه لمعلومة معينة."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Typographic Conventions"
msgstr "الأعراف الطباعية"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Four typographic conventions are used to call attention to specific words and phrases. These conventions, and the circumstances they apply to, are as follows."
msgstr "هناك أربع أعراف طباعية تستخدم في جلب الانتباه لكلمات أو عبارات معينة. إليك تاليا الأعراف والظروف التي تنطبق عليها."

msgid "<literal>Mono-spaced Bold</literal>"
msgstr ""

msgid "Used to highlight system input, including shell commands, file names and paths. Also used to highlight keys and key combinations. For example:"
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "To see the contents of the file <filename>my_next_bestselling_novel</filename> in your current working directory, enter the <command>cat my_next_bestselling_novel</command> command at the shell prompt and press <keycap>Enter</keycap> to execute the command."
msgstr "لمشاهدة محتويات الملف <filename>my_next_bestselling_novel</filename> الموجود في الدليل (المجلد) الحالي اكتب الأمر‪ <command>cat my_next_bestselling_novel</command> ‬في المحث واضغط <keycap>Enter</keycap>لتنفيذ الأمر."

msgid "The above includes a file name, a shell command and a key, all presented in mono-spaced bold and all distinguishable thanks to context."
msgstr ""

msgid "Key combinations can be distinguished from an individual key by the plus sign that connects each part of a key combination. For example:"
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Press <keycap>Enter</keycap> to execute the command."
msgstr "اضغط <keycap>ENTER</keycap> لتنفيذ الأمر."

msgid "Press <keycombo><keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>F2</keycap></keycombo> to switch to a virtual terminal."
msgstr ""

msgid "The first example highlights a particular key to press. The second example highlights a key combination: a set of three keys pressed simultaneously."
msgstr ""

msgid "If source code is discussed, class names, methods, functions, variable names and returned values mentioned within a paragraph will be presented as above, in <literal>mono-spaced bold</literal>. For example:"
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "File-related classes include <classname>filesystem</classname> for file systems, <classname>file</classname> for files, and <classname>dir</classname> for directories. Each class has its own associated set of permissions."
msgstr "الصنوف المتعلقة بالملفات تتضمن <classname>filesystem</classname> لنظام الملفات، <classname>file</classname> للملفات، و <classname>dir</classname> للأدلة (المجلدات)، وكل صنف له ما يرتبط به من الصلاحيات."

msgid "<application>Proportional Bold</application>"
msgstr ""

msgid "This denotes words or phrases encountered on a system, including application names; dialog-box text; labeled buttons; check-box and radio-button labels; menu titles and submenu titles. For example:"
msgstr ""

msgid "Choose <menuchoice><guimenu>System</guimenu><guisubmenu>Preferences</guisubmenu><guimenuitem>Mouse</guimenuitem></menuchoice> from the main menu bar to launch <application>Mouse Preferences</application>. In the <guilabel>Buttons</guilabel> tab, select the <guilabel>Left-handed mouse</guilabel> check box and click <guibutton>Close</guibutton> to switch the primary mouse button from the left to the right (making the mouse suitable for use in the left hand)."
msgstr ""

msgid "To insert a special character into a <application>gedit</application> file, choose <menuchoice><guimenu>Applications</guimenu><guisubmenu>Accessories</guisubmenu><guimenuitem>Character Map</guimenuitem></menuchoice> from the main menu bar. Next, choose <menuchoice><guimenu>Search</guimenu><guimenuitem>Find…</guimenuitem></menuchoice> from the <application>Character Map</application> menu bar, type the name of the character in the <guilabel>Search</guilabel> field and click <guibutton>Next</guibutton>. The character you sought will be highlighted in the <guilabel>Character Table</guilabel>. Double-click this highlighted character to place it in the <guilabel>Text to copy</guilabel> field and then click the <guibutton>Copy</guibutton> button. Now switch back to your document and choose <menuchoice><guimenu>Edit</guimenu><guimenuitem>Paste</guimenuitem></menuchoice> from the <application>gedit</application> menu bar."
msgstr ""

msgid "The above text includes application names; system-wide menu names and items; application-specific menu names; and buttons and text found within a GUI interface, all presented in proportional bold and all distinguishable by context."
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "<command><replaceable>Mono-spaced Bold Italic</replaceable></command> or <application><replaceable>Proportional Bold Italic</replaceable></application>"
msgstr "<command><replaceable>ثخين ثابت العرض مائل Mono-spaced Bold Italic</replaceable></command> أو <application><replaceable>ثخين متناسب مائل  Proportional Bold Italic</replaceable></application>"

msgid "Whether mono-spaced bold or proportional bold, the addition of italics indicates replaceable or variable text. Italics denotes text you do not input literally or displayed text that changes depending on circumstance. For example:"
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "To connect to a remote machine using ssh, type <command>ssh <replaceable>username</replaceable>@<replaceable>domain.name</replaceable></command> at a shell prompt. If the remote machine is <filename>example.com</filename> and your username on that machine is john, type <command>ssh john@example.com</command>."
msgstr "للاتصال بجهاز بعيد عبر ssh، اكتب <command>ssh <replaceable>username</replaceable>@<replaceable>domain.name</replaceable></command> في محث سطر الأوامر. فإن كان الجهاز البعيد هو <filename>example.com</filename> واسمك عليه هو john، اكتب <command>ssh john@example.com</command>."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "The <command>mount -o remount <replaceable>file-system</replaceable></command> command remounts the named file system. For example, to remount the <filename>/home</filename> file system, the command is <command>mount -o remount /home</command>."
msgstr "الأمر <command>mount -o remount <replaceable>file-system</replaceable></command> يعمل على إعادة ضم نظام الملفات المسمى. مثلا، لإعادة ضم نظام الملفات   ‪ <filename>/home</filename> ‬  الأمر هو <command>mount -o remount /home</command>."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "To see the version of a currently installed package, use the <command>rpm -q <replaceable>package</replaceable></command> command. It will return a result as follows: <command><replaceable>package-version-release</replaceable></command>."
msgstr "لمعرفة إصدار حزمة مثبتة استخدم الأمر <command>rpm -q <replaceable>package</replaceable></command>وهو يعطيك: <command><replaceable>package-version-release</replaceable></command>."

msgid "Note the words in bold italics above: username, domain.name, file-system, package, version and release. Each word is a placeholder, either for text you enter when issuing a command or for text displayed by the system."
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Aside from standard usage for presenting the title of a work, italics denotes the first use of a new and important term. For example:"
msgstr "وبعيدا عن الاستخدام القياسي لتمثيل العناوين، المائل يستخدم لتعين أول ظهور لحد (أي مصطلح) جديد. مثلا:"

msgid "Publican is a <firstterm>DocBook</firstterm> publishing system."
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Pull-quote Conventions"
msgstr "أعراف الاقتباسات المقتطعة Pull-quote"

msgid "Terminal output and source code listings are set off visually from the surrounding text."
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Output sent to a terminal is set in <computeroutput>mono-spaced roman</computeroutput> and presented thus:"
msgstr "مخرجات مرسلة للطرفية تُصف <computeroutput>بالخط الرومي ثابت العرض c  </computeroutput>Mono-spaced Roman  لذا تظهر هكذا:"

msgid "books        Desktop   documentation  drafts  mss    photos   stuff  svn\n"
"books_tests  Desktop1  downloads      images  notes  scripts  svgs"
msgstr ""

msgid "Source-code listings are also set in <computeroutput>mono-spaced roman</computeroutput> but add syntax highlighting as follows:"
msgstr ""

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Notes and Warnings"
msgstr "الملاحظات والتحذيرات"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Finally, we use three visual styles to draw attention to information that might otherwise be overlooked."
msgstr "أخيرا، نستخدم ثلاث تنسيقات مرئية لجلب الانتباه إلى المعلومات التي قد تخطؤها العين."

msgid "Notes are tips, shortcuts or alternative approaches to the task at hand. Ignoring a note should have no negative consequences, but you might miss out on a trick that makes your life easier."
msgstr ""

msgid "Important boxes detail things that are easily missed: configuration changes that only apply to the current session, or services that need restarting before an update will apply. Ignoring a box labeled “Important” will not cause data loss but may cause irritation and frustration."
msgstr ""

msgid "Warnings should not be ignored. Ignoring warnings will most likely cause data loss."
msgstr ""

