# AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2013-09-02 06:46-0400\n"
"Last-Translator: gcintra <gcintra@fedoraproject.org>\n"
"Language-Team: None\n"
"Language: pt-BR\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.6.0\n"

# translation auto-copied from project Publican Common, version 3, document Revision_History
msgid "Revision History"
msgstr "Histórico de Revisão"

msgid "Jeff"
msgstr "Jeff"

msgid "Fearn"
msgstr "Fearn"

msgid "Publican 3.0"
msgstr "Publican 3.0"

