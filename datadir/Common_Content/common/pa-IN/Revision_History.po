# AUTHOR <EMAIL@ADDRESS>, YEAR.
# jassy <jassy@fedoraproject.org>, 2012. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2012-08-31 05:48-0400\n"
"Last-Translator: jassy <jassy@fedoraproject.org>\n"
"Language-Team: None\n"
"Language: pa\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.6.0\n"

# translation auto-copied from project Publican Common, version 3, document Revision_History
msgid "Revision History"
msgstr "ਦੁਹਰਾਈ ਅਤੀਤ"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author jassy
msgid "Jeff"
msgstr "Jeff"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author jassy
msgid "Fearn"
msgstr "Fearn"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author jassy
msgid "Publican 3.0"
msgstr "Publican 3.0"

