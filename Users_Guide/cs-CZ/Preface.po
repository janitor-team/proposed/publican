# Josef Hruška <josef.hruska@upcmail.cz>, 2014. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2014-10-03 13:17+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2014-10-07 01:01-0400\n"
"Last-Translator: Josef Hruška <josef.hruska@upcmail.cz>\n"
"Language-Team: Czech\n"
"Language: cs\n"
"X-Generator: Zanata 3.6.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2\n"

msgid "Preface"
msgstr "Předmluva"

msgid "We Need Feedback!"
msgstr "Potřebujeme zpětnou vazbu!"

msgid "<primary>feedback</primary> <secondary>contact information for this manual</secondary>"
msgstr "<primary>zpětná vazba</primary> <secondary>kontaktní informace pro tento manuál</secondary>"

msgid "If you find a typographical error in this manual, or if you have thought of a way to make this manual better, we would love to hear from you! Please submit a report in Bugzilla: <uri xlink:href=\"https://bugzilla.redhat.com/enter_bug.cgi?product=Publican&amp;component=Publican%20Users%20Guide&amp;version=4.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">https://bugzilla.redhat.com/enter_bug.cgi?product=Publican&amp;component=Publican%20Users%20Guide&amp;version=4.1</uri>."
msgstr "Naleznete-li v tomto manuálu typografickou chybu nebo přemýšlíte-li nad způsobem jak jej učinit lepším, velmi rádi si vás vyslechneme! Prosíme, podejte hlášení do Bugzilly: <uri xlink:href=\"https://bugzilla.redhat.com/enter_bug.cgi?product=Publican&amp;component=Publican%20Users%20Guide&amp;version=4.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">https://bugzilla.redhat.com/enter_bug.cgi?product=Publican&amp;component=Publican%20Users%20Guide&amp;version=4.1</uri>."

msgid "If you have a suggestion for improving the documentation, try to be as specific as possible when describing it. If you have found an error, please include the section number and some of the surrounding text so we can find it easily."
msgstr "Máte-li návrh na vylepšení dokumentace, pokuste se být co nejpřesnější při jeho popisu. Pokud jste nalezli chybu, prosíme uveďte číslo oddílu (kapitoly) a trochu okolního textu, ať ji můžeme snáze nalézt."

