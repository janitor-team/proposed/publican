# Gé Baylard <<Geodebay@gmail.com>>, 2013.
#
#
# Gé Baylard <geodebay@gmail.com>, 2014. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2014-10-03 13:17+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2014-05-23 04:44-0400\n"
"Last-Translator: Gé Baylard <geodebay@gmail.com>\n"
"Language-Team: français <<trans-fr@lists.fedoraproject.org>>\n"
"Language: fr\n"
"X-Generator: Zanata 3.6.2\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"

msgid "Creating, installing, and updating the home page"
msgstr "Création, installation et mise à jour de la page d'accueil"

msgid "The <application>Publican</application>-generated home page is the localizable page to which visitors are directed by the site JavaScript and which provides the style for the website structure. The home page is structured as a DocBook <tag>&lt;article&gt;</tag> with an extra <parameter>web_type: home</parameter> parameter in its <filename>publican.cfg</filename> file. In its structure and its presentation, the home page is the same as any other article that you produce with <application>Publican</application>. To create the home page:"
msgstr "La page d'accueil créée par <application>Publican</application> est la page adaptable à la langue du lecteur vers laquelle les visiteurs sont dirigés par le JavaScript du site ; elle fournit également le style de l'architecture du site. La page d'accueil est structurée comme un <sgmltag>&lt;article&gt;</sgmltag> DocBook avec un paramètre <parameter>web_type:&nbsp;home</parameter> supplémentaire dans son fichier <filename>publican.cfg</filename>. La structure et la présentation de la page d'accueil est la même que pour tout autre article produit avec <application>Publican</application>. Pour créer la page d'accueil :"

msgid "Change into a convenient directory and run the following <prompt>$</prompt> <command>publican create</command> command:"
msgstr "Mettez vous dans un répertoire adapté et lancez la commande <prompt>$</prompt> <command>publican create</command> :"

msgid "<prompt>$</prompt> <command>publican create --type Article --name <replaceable>page_name</replaceable></command>"
msgstr "<prompt>$</prompt> <command>publican create --type Article --name <replaceable>nom_page</replaceable></command>"

msgid "For example:"
msgstr "Par exemple :"

msgid "<prompt>$</prompt> <command>publican create --type Article --name Home_Page</command>"
msgstr "<prompt>$</prompt> <command>publican create --type Article --name Home_Page</command>"

msgid "Most brands (including the <literal>common</literal> brand) present the name of the document in large, coloured letters close to the top of the page, underneath the banner that contains the product name (the <option>--name</option> option sets the <tag>&lt;title&gt;</tag> tag). Therefore, by default, the value that you set with the <option>--name</option> option is presented prominently to visitors to your site; in the above example, visitors are greeted with the words <literal>Home Page</literal> underneath the product banner."
msgstr "La plupart des estampillages (y compris l'estampille <literal>common</literal>) présentent le nom du document en grandes lettres de couleur près du haut de la page, au dessous de la bannière comportant le nom du produit (l'option <option>--name</option> définit la balise <sgmltag>&lt;title&gt;</sgmltag>). Ainsi, par défaut, la valeur que vous indiquez avec l'option <option>--name</option> est présentée bien en évidence aux visiteurs du site ; dans l'exemple ci-dessus, les visiteurs sont accueillis par les mots <literal>Home Page</literal> juste au dessous de la bannière du produit."

msgid "Change into the article directory:"
msgstr "Placez-vous dans le répertoire de l'article :"

msgid "<prompt>$</prompt> <command>cd <replaceable>page_name</replaceable></command>"
msgstr "<prompt>$</prompt> <command>cd <replaceable>nom_page</replaceable></command>"

msgid "<prompt>$</prompt> <command>cd Home_Page</command>"
msgstr "<prompt>$</prompt> <command>cd Home_Page</command>"

msgid "Unlink the <filename>Article_Info.xml</filename> file from your root XML file."
msgstr "Enlevez le lien avec le fichier <filename>Article_Info.xml</filename> dans votre fichier racine XML."

msgid "Little of the content of the <filename>Article_Info.xml</filename> file is likely to be useful for the home page of your website. Therefore, edit the root XML file of your home page to remove the <tag>&lt;xi:include&gt;</tag> tag that links to <filename>Article_Info.xml</filename>. <application>Publican</application> still uses the information in <filename>Article_Info.xml</filename> for packaging, but does not include it on the page itself."
msgstr "Pratiquement rien du contenu du fichier <filename>Article_Info.xml</filename> n'est susceptible d'avoir une utilité pour la page d'accueil de votre site Web. Ainsi, modifiez le fichier racine XML de votre page d'accueil pour supprimer la balise <sgmltag>&lt;xi:include&gt;</sgmltag> liant à <filename>Article_Info.xml</filename>. <application>Publican</application> utilise toujours des informations situées dans <filename>Article_Info.xml</filename> pour l'empaquetage, mais il ne les incorpore pas dans la page elle-même."

msgid "Edit the <filename>publican.cfg</filename> file."
msgstr "Modifiez le fichier <filename>publican.cfg</filename>."

msgid "At the very least, you must add the <parameter>web_type</parameter> parameter and set it to <literal>home</literal>:"
msgstr "Tout à fait à la fin, vous devez ajouter le paramètre <parameter>web_type</parameter> et lui donner la valeur <literal>home</literal> :"

msgid "web_type: home"
msgstr "web_type: home"

msgid "The <parameter>web_type: home</parameter> parameter instructs <application>Publican</application> to process this document differently from product documentation. This is the only mandatory change to the <filename>publican.cfg</filename> file. Other optional changes to the <filename>publican.cfg</filename> file that are frequently useful for <application>Publican</application>-generated websites include:"
msgstr "Le paramètre <parameter>web_type:&nbsp;home</parameter> indique à <application>Publican</application> de traiter le document différemment d'une documentation de produit. C'est le seul changement obligatoire dans le fichier <filename>publican.cfg</filename>. Voici d'autres modifications facultatives de <filename>publican.cfg</filename>, souvent utiles pour les sites Web créés avec <application>Publican</application> :"

msgid "<parameter>brand</parameter>"
msgstr "<parameter>brand</parameter>"

msgid "To style your home page to match your documents, add:"
msgstr "Pour harmoniser le style de la page d'accueil avec vos documents, ajoutez :"

msgid "brand: <replaceable>name_of_brand</replaceable>"
msgstr "brand: <replaceable>nom_estampille</replaceable>"

msgid "<parameter>docname</parameter>"
msgstr "<parameter>docname</parameter>"

msgid "<parameter>product</parameter>"
msgstr "<parameter>product</parameter>"

msgid "If the <tag>&lt;title&gt;</tag> or the <tag>&lt;product&gt;</tag> that you set in the <filename>Article_Info</filename> file included anything other than basic, unaccented Latin characters, set the <parameter>docname</parameter> and <parameter>product</parameter> as necessary."
msgstr "Dans les balises <sgmltag>&lt;title&gt;</sgmltag> ou <sgmltag>&lt;product&gt;</sgmltag> définies dans le fichier <filename>Article_Info</filename> ne comportant rien d'autre que des caractères latins de base sans accents, définissez les paramètres <parameter>docname</parameter> et <parameter>product</parameter> comme il convient."

msgid "Edit the content of the <filename><replaceable>page_name</replaceable>.xml</filename> file (for example, <filename>Home_Page.xml</filename>) as you would any other DocBook document."
msgstr "Modifiez le contenu du fichier <filename><replaceable>nom_page</replaceable>.xml</filename> (par exemple, <filename>Home_Page.xml</filename>) comme vous le feriez pour tout autre document DocBook."

msgid "If you remove the <tag>&lt;xi:include&gt;</tag> that links to <filename>Article_Info.xml</filename>, specify a title for your page in the following format:"
msgstr "Si vous supprimez la balise <sgmltag>&lt;xi:include&gt;</sgmltag> liant à <filename>Article_Info.xml</filename>, donnez un titre pour votre page avec le format :"

msgid "&lt;title role=\"producttitle\"&gt;FooMaster Documentation&lt;/title&gt;"
msgstr "&lt;title role=\"producttitle\"&gt;Documentation de FooMaster&lt;/title&gt;"

msgid "If you publish documentation in more than one language, create a set of POT files and a set of PO files for each language with the <prompt>$</prompt> <command>publican update_pot</command> and <command>publican update_po</command> commands."
msgstr "Si vous publiez la documentation en plus d'une langue, créez un ensemble de fichiers POT et des ensembles de fichiers PO pour chaque langue avec les commandes <prompt>$</prompt> <command>publican update_pot</command> et <command>publican update_po</command>."

msgid "To customize the logo at the top of the navigation menu that provides a link back to the home page, create a PNG image 290 px × 100 px and name it <filename>web_logo.png</filename>. Place this image in the <filename>images/</filename> directory in the document's XML directory, for example <filename>en-US/images/</filename>."
msgstr "Pour personnaliser le logo en tête du menu de navigation et le lier à la page d'accueil, créez une image PNG de 290&nbsp;px&nbsp;×&nbsp;100&nbsp;px et appelez-la <filename>web_logo.png</filename>. Placez cette image dans le sous-répertoire <filename>images/</filename> du répertoire XML du document, par exemple, <filename>en-US/images/</filename>."

msgid "To specify site-specific styles to override the styles set in the website's <filename>interactive.css</filename> file, add styles to a file named <filename>site_overrides.css</filename> and place it in the root of your document source (the same directory that contains <filename>publican.cfg</filename> and the language directories)."
msgstr "Pour définir des styles propres au site prenant le pas sur les styles définis dans le fichier <filename>interactive.css</filename>, ajoutez des styles dans un fichier nommé <filename>site_overrides.css</filename> et placez-le dans la racine de votre source de documents (le répertoire contenant <filename>publican.cfg</filename> et les répertoires de langues)."

msgid "Build the home page in single-page HTML format with the <option>--embedtoc</option> option and install it in your website structure. For example:"
msgstr "Construisez la page d'accueil au format HTML page unique avec l'option <option>--embedtoc</option> et installez-la dans la structure du site Web. Par exemple :"

msgid "<prompt>$</prompt> <command>publican build --publish --formats html-single --embedtoc --langs all</command> \n"
"<prompt>$</prompt> <command>publican install_book --site_config ~/docsite/foomaster.cfg --lang <replaceable>Language_Code</replaceable></command>"
msgstr "<prompt>$</prompt> <command>publican build --publish --formats html-single --embedtoc --langs all</command> \n"
"<prompt>$</prompt> <command>publican install_book --site_config ~/docsite/foomaster.cfg --lang <replaceable>Code_langue</replaceable></command>"

msgid "Note that you can build all languages at the same time, but must install the home page for each language with a separate <prompt>$</prompt> <command>publican install_book</command> command."
msgstr "Notez que vous pouvez construire toutes les langues simultanément, mais vous devez installer la page d'accueil pour chaque langue à l'aide d'une commande <prompt>$</prompt> <command>publican install_book</command> distincte."

